package Topic3;

public class Assignment2 {
public static void main(String[] args) {
	//command line arguments name1 marks1 marks2 marks3 name2 marks1 marks2 marks3
	try {
		float average1=(float)(Integer.parseInt(args[1])+Integer.parseInt(args[2])+Integer.parseInt(args[3]))/3;
		float average2=(float)(Integer.parseInt(args[5])+Integer.parseInt(args[6])+Integer.parseInt(args[7]))/3;
		System.out.println(args[0]+" : "+average1);
		System.out.println(args[4]+" : "+average2);
	}catch (NumberFormatException e) {
		// TODO: handle exception
		System.out.println("Only integers are allowed");
	}
}
}
