package Topic1;

public class Assignment7 {
	//using recursion
	static int factorial(int i) {
		if(i==0)
			return 1;
		else return i*factorial(i-1);
	}
public static void main(String[] args) {
	System.out.println(factorial(10));
}
}
