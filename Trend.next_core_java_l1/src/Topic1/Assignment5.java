package Topic1;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Assignment5 {
public static void main(String[] args) throws Exception{
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	//reading number as string
	String number=br.readLine();
	
	// character 0 is equal to 48 in decimal so we have to subtract 48 for each character i.e. 48*4
	int sum=number.charAt(0)+number.charAt(1)+number.charAt(2)+number.charAt(3)-(48*4);
	System.out.print(sum);
	
}
}
