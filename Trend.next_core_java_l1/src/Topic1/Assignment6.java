package Topic1;

public class Assignment6 {
public static void main(String[] args) {
	int[] arr= {2,5,7,9,1,0};
	int max=arr[0];
	
	//update max for every element of array based on if condition 
	for(int i:arr) {
		if(i>=max)
			max=i;
	}
	System.out.print(max);
}
}
