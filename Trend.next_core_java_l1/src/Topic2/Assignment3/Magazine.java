package Topic2.Assignment3;

public class Magazine extends Book{
private String type;

public Magazine(String isbn, String title, float price, String type) {
	super(isbn, title, price);
	this.type = type;
}
void displayDetails() {
	displayCommon();
	System.out.print("type : "+type);
}

}
