package Topic2.Assignment5;

public class Implementation {
public static void main(String[] args) {
	Instrument instruments[]=new Instrument[10];
	instruments[0]=new Piano();
	instruments[1]=new Flute();
	instruments[2]=new Guitar();
	instruments[3]=new Piano();
	instruments[4]=new Flute();
	instruments[5]=new Guitar();
	instruments[6]=new Piano();
	instruments[7]=new Flute();
	instruments[8]=new Guitar();
	instruments[9]=new Piano();
	for(Instrument i:instruments) {
		i.play();
		
	}
	int index=0;
	for(Instrument i:instruments) {
		if(i instanceof Piano) {
			System.out.println("index "+index+" is an instance of Piano");
		}
		else if(i instanceof Flute) {
			System.out.println("index "+index+" is an instance of Flute");
		}
		else if(i instanceof Guitar) {
			System.out.println("index "+index+" is an instance of Guitar");
		}
		index++;
	}

	
}
}
