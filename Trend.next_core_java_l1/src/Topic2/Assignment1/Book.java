package Topic2.Assignment1;

public class Book {
private String isbn;
private String title;
private String author;
private float price;
public Book(String isbn, String title, String author, float price) {
	super();
	this.isbn = isbn;
	this.title = title;
	this.author = author;
	this.price = price;
}
void displydetails() {
	System.out.print("isbn : "+isbn+"\n"+"title : "+title+"\n"+"author : "+author+"\n"+"price : "+price+"\n");
}
float discountedprice(float percentage) {
	return price-(price*percentage/100);
}

public static void main(String[] args) {
	Book book=new Book("AB20012","The Adventures of Sherlock Holmes","Arthur Conan Doyle",500);
	book.displydetails();
	System.out.print("discounted price : "+book.discountedprice(20));
}


}
