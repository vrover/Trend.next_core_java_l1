package Topic4;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;

public class Assignment4 {
public static void main(String[] args) throws Exception {
	HashMap<String,Integer> h=new HashMap<String,Integer>();
	h.put("Bruce", 687636);
	h.put("Amanda",56733);
	h.put("Kaley",68764);
	h.put("Sheldon",56236);
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	String name=br.readLine();
	System.out.print(h.get(name));
}
}
