package Topic4;

import java.util.Random;



public class Assignment1 {
	int i;
	
	//synchronized method to get a random number (0-10) and print it
	synchronized void randomNumber() {
		Random rand=new Random();
		for(int j=0;j<5;j++) {
			
		i=rand.nextInt(10);
		
		System.out.println("Number : "+i);
		notify();
		 try {wait();} catch(Exception e) {}
		}
		
	}
	
	//function that takes a number as argument and return factorial
	 int factorial(int i) {
		
		if(i==0)
			return 1;
		else return i*factorial(i-1);
	}

	 //synchronized method to print the factorial of a number and waits for the generation of another random number
	 synchronized void printFactorial(){
		 for(int j=0;j<5;j++) {
		 System.out.println("Factorial : "+factorial(i));
		 notify();
		 try {wait();} catch(Exception e) {}
		 }
	 }
public static void main(String[] args) {
	
	Assignment1 obj=new Assignment1();
	
	//thread to print a random number
	Thread t1=new Thread() {
		public void run() {
			obj.randomNumber();
		}
	};
	
	//thread to print factorial
	Thread t2=new Thread() {
		public void run() {
			obj.printFactorial();
		}
	};


		//starting both the threads
		t1.start();
		t2.start();

	
}
}
