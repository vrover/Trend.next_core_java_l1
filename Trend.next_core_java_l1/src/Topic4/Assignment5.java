package Topic4;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;

public class Assignment5 {
public static void main(String[] args) {
	HashSet<String> h=new HashSet<String>();
	String[] names={"Susan","Hayden","Alex","Tony","Bruce","Kate"};
	h.addAll(Arrays.asList(names));
	Iterator<String> itr=h.iterator();
	while(itr.hasNext()) {
		System.out.println(itr.next());
	}
} 
}
