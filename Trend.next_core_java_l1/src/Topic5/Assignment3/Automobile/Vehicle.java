package Topic5.Assignment3.Automobile;

public abstract class Vehicle {
abstract public String modelName();
abstract public String registrationNumber();
abstract public String ownerName();
}
