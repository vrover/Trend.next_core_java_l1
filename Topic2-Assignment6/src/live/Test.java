package live;

import music.string.Veena;
import music.wind.Saxophone;
import music.Playable;

public class Test {
public static void main(String[] args) {
	Veena veena=new Veena();
	Saxophone saxophone=new Saxophone();
	veena.play();
	saxophone.play();
	
	//changing type
	Playable v=veena;
	Playable s=saxophone;
	v.play();
	s.play();
	
	
}
}
